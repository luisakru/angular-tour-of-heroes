import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';  //app can have routing functionality
import { HeroesComponent } from './heroes/heroes.component'; //will give the Router somewhere to go once you configure the routes.
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';

// The next part of the file is where you configure your routes. Routes tell the Router
// which view to display when a user clicks a link or pastes a URL into the browser address bar.
const routes: Routes = [
  { path: 'heroes', component: HeroesComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: HeroDetailComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' } //To make the app navigate to the dashboard automatically
];

// 1. The following line adds the RouterModule to the AppRoutingModule imports array and
// configures it with the routes in one step by calling RouterModule.forRoot():
// 2. exports RouterModule so it will be available throughout the app
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }